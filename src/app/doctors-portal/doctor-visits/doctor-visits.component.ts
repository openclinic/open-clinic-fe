import { Component, OnInit } from "@angular/core";
import { User } from "../../authentication/model/user";
import { VisitService } from "../../services/visit.service";
import { Visit } from "../../model/visit";
import { VisitStatus } from "../../model/visitStatus";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { MatDialog } from "@angular/material/dialog";
import { ClinicService } from "../../services/clinic.service";
import { ClinicDetailsDialogComponent } from "../../dialogs/clinic-details-dialog/clinic-details-dialog.component";
import { PaginatedDataSource } from "../../data-source/pagination-data-source";
import { AuthenticationService } from "src/app/authentication/authentication.service";
import { Router } from "@angular/router";
import { DiagnoseDialogComponent } from "src/app/dialogs/diagnose-dialog/diagnose-dialog.component";

@Component({
  selector: "app-doctor-visits",
  templateUrl: "./doctor-visits.component.html",
  styleUrls: ["./doctor-visits.component.scss"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class DoctorVisitsComponent implements OnInit {
  user: User;

  expandedVisit: any;

  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty("detailRow");

  displayedColumns: string[] = [
    "Patient",
    "Clinic",
    "Start Date",
    "End Date",
    "Description",
    "Price/USD",
    "Status",
  ];
  dataSource: any;
  endpoint: any;

  constructor(
    private router: Router,
    private visitService: VisitService,
    private clinicService: ClinicService,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
    this.endpoint = (request) =>
      this.visitService.getAllForDoctor(request, this.user.uuid);

    this.dataSource = new PaginatedDataSource<Visit>(this.endpoint, {
      property: "startDate",
      order: "desc",
    });
  }

  cancelVisit(visit: Visit): void {
    this.visitService
      .updateStatus(visit.uuid, VisitStatus.Canceled)
      .subscribe(() => {
        this.dataSource.fetch(this.dataSource.currentPageNumber);
      });
  }

  finishVisit(visit: Visit): void {
    this.visitService
      .updateStatus(visit.uuid, VisitStatus.Finished)
      .subscribe(() => {
        this.dataSource.fetch(this.dataSource.currentPageNumber);
      });
  }

  openPatientDocumentation(visit: Visit): void {
    this.router.navigate(["/doctor-portal/patient-documentation"], {
      queryParams: { isEditable: false, patientUUID: visit.patient.uuid },
      queryParamsHandling: "merge",
    });
  }

  openClinicDialog(visit: Visit): void {
    this.clinicService.getByUUID(visit.clinic.uuid).subscribe((clinic) => {
      this.dialog.open(ClinicDetailsDialogComponent, {
        width: "500px",
        height: "500px",
        data: clinic,
      });
    });
  }

  openDiagnoseDialog(visit: Visit): void {
    this.dialog.open(DiagnoseDialogComponent, {
      width: "500px",
      height: "550px",
      data: {
        doctorUUID: this.user.uuid,
        patientUUID: visit.patient.uuid,
        visitUUID: visit.uuid,
      },
    });
  }

  onSort(event: any) {
    this.dataSource.sortBy({
      property: event.active,
      order: event.direction,
    });
  }
}
