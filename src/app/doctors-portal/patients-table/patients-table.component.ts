import { Component, OnInit, Input } from "@angular/core";
import { User } from "../../authentication/model/user";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { MatDialog } from "@angular/material/dialog";
import { PaginatedDataSource } from "../../data-source/pagination-data-source";
import { PatientService } from "src/app/services/patient.service";
import { Patient } from "src/app/model/patient";
import { Router } from "@angular/router";
import { PrescribingDialogComponent } from "src/app/dialogs/prescribing-dialog/prescribing-dialog.component";

@Component({
  selector: "app-patients-table",
  templateUrl: "./patients-table.component.html",
  styleUrls: ["./patients-table.component.scss"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class PatientsTableComponent implements OnInit {
  @Input()
  user: User;

  expandedPatient: any;

  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty("detailRow");

  displayedColumns: string[] = [
    "Patient",
    "Email",
    "Date Of Joining",
    "Identity Number",
    "Phone Number",
  ];
  dataSource: any;
  endpoint: any;

  constructor(
    private patientService: PatientService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.endpoint = (request) =>
      this.patientService.getPatientsForDoctor(request, this.user.uuid);
    this.dataSource = new PaginatedDataSource<Patient>(this.endpoint, {
      property: "dateOfJoining",
      order: "desc",
    });
  }

  openPatientsDocumentation(patient: Patient): void {
    this.router.navigate(["/doctor-portal/patient-documentation"], {
      queryParams: { isEditable: false, patientUUID: patient.uuid },
      queryParamsHandling: "merge",
    });
  }

  openPrescribingDialog(patient: Patient): void {
    this.dialog.open(PrescribingDialogComponent, {
      width: "500px",
      height: "550px",
      data: {
        doctorUUID: this.user.uuid,
        patientUUID: patient.uuid,
      },
    });
  }

  openPatientsPrescriptions(patient: Patient): void {
    this.router.navigate(["/doctor-portal/prescriptions"], {
      queryParams: {
        isUser: false,
        isDoctor: false,
        patientUUID: patient.uuid,
      },
      queryParamsHandling: "merge",
    });
  }

  onSort(event: any) {
    this.dataSource.sortBy({
      property: event.active,
      order: event.direction,
    });
  }
}
