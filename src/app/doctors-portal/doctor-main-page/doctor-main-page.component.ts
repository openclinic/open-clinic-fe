import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../../authentication/authentication.service";

@Component({
  selector: "app-doctor-main-page",
  templateUrl: "./doctor-main-page.component.html",
  styleUrls: ["./doctor-main-page.component.scss"],
})
export class DoctorMainPageComponent implements OnInit {
  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit() {}

  logout() {
    this.authenticationService.logout();
  }
}
