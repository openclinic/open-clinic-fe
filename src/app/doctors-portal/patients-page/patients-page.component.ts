import { Component, OnInit } from "@angular/core";
import { User } from "src/app/authentication/model/user";
import { AuthenticationService } from "src/app/authentication/authentication.service";

@Component({
  selector: "app-patients-page",
  templateUrl: "./patients-page.component.html",
  styleUrls: ["./patients-page.component.scss"],
})
export class PatientsPageComponent implements OnInit {
  user: User;

  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
  }
}
