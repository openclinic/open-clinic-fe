import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/authentication/authentication.service";
import { MatDialog } from "@angular/material/dialog";
import { EditUserDialogComponent } from "src/app/dialogs/edit-user-dialog/edit-user-dialog.component";
import { PhotoDialogComponent } from "src/app/dialogs/photo-dialog/photo-dialog.component";
import { Doctor } from "src/app/model/doctor";
import { DoctorService } from "src/app/services/doctor.service";

@Component({
  selector: "app-doctor-profile-page",
  templateUrl: "./doctor-profile-page.component.html",
  styleUrls: ["./doctor-profile-page.component.scss"],
})
export class DoctorProfilePageComponent implements OnInit {
  doctor: Doctor;
  base64Data: any;
  retrievedImage: any;
  isEditable: any;

  constructor(
    private authentiationService: AuthenticationService,
    private doctorService: DoctorService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    let doctorUUID = this.authentiationService.currentUserValue.uuid;
    this.doctorService.getByUUID(doctorUUID).subscribe((doctor) => {
      this.doctor = doctor;
      this.base64Data = this.doctor.photo;
      this.retrievedImage = "data:image/jpeg;base64," + this.base64Data;
    });
  }

  openEditDialog(): void {
    this.dialog.open(EditUserDialogComponent, {
      width: "500px",
      height: "550px",
      data: this.doctor,
    });
  }

  openPhotoDialog(): void {
    this.dialog.open(PhotoDialogComponent, {
      width: "500px",
      height: "300px",
      data: this.doctor,
    });
  }
}
