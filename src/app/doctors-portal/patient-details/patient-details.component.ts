import { Component, OnInit, Input } from "@angular/core";
import { Patient } from "src/app/model/patient";

@Component({
  selector: "app-patient-details",
  templateUrl: "./patient-details.component.html",
  styleUrls: ["./patient-details.component.scss"],
})
export class PatientDetailsComponent implements OnInit {
  @Input()
  patient: Patient;

  base64Data: any;
  retrievedImage: any;

  ngOnInit() {
    this.base64Data = this.patient.photo;
    this.retrievedImage = "data:image/jpeg;base64," + this.base64Data;
  }
}
