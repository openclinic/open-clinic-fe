import { Component, OnInit, AfterViewInit } from "@angular/core";
import { AuthenticationService } from "../../authentication/authentication.service";
import { FocusMonitor } from "@angular/cdk/a11y";
import { DoctorService } from "src/app/services/doctor.service";
@Component({
  selector: "app-doctor-header",
  templateUrl: "./doctor-header.component.html",
  styleUrls: ["./doctor-header.component.scss"],
})
export class DoctorHeaderComponent implements OnInit, AfterViewInit {
  base64Data: any;
  retrievedImage: any;

  constructor(
    private authenticationService: AuthenticationService,
    private doctorService: DoctorService,
    private _focusMonitor: FocusMonitor
  ) {}

  ngOnInit() {
    this.doctorService
      .getByUUID(this.authenticationService.currentUserValue.uuid)
      .subscribe((user) => {
        this.base64Data = user.photo;
        this.retrievedImage = "data:image/jpeg;base64," + this.base64Data;
      });
  }

  ngAfterViewInit() {
    this._focusMonitor.stopMonitoring(
      document.getElementById("sidenav_button")
    );
  }

  logout() {
    this.authenticationService.logout();
  }
}
