import { PaginatedDataSource } from "../data-source/pagination-data-source";
import { Component, Input } from "@angular/core";
import { User } from "../authentication/model/user";
import { PrescriptionService } from "../services/prescription.service";
import { Prescription } from "../model/prescription";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";

@Component({
  selector: "app-prescriptions-table",
  templateUrl: "./prescriptions-table.component.html",
  styleUrls: ["./prescriptions-table.component.scss"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class PrescriptionsTableComponent {
  @Input()
  user: User;

  @Input()
  isDoctor: any;

  expandedPrescription: any;

  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty("detailRow");

  displayedColumns: string[] = [
    "Medicine",
    "Description",
    "Issuing Date",
    "Expiration Date",
    "Code",
    "Patient",
    "Doctor",
  ];
  dataSource: any;
  endpoint: any;

  constructor(private prescriptionService: PrescriptionService) {}

  ngOnInit() {
    if (this.isDoctor === "true") {
      this.endpoint = (request) =>
        this.prescriptionService.getAllByDoctorUUID(request, this.user.uuid);
    } else {
      this.endpoint = (request) =>
        this.prescriptionService.getAllByPatientUUID(request, this.user.uuid);
    }

    this.dataSource = new PaginatedDataSource<Prescription>(this.endpoint, {
      property: "dateOfIssuing",
      order: "desc",
    });
  }

  onSort(event: any) {
    this.dataSource.sortBy({
      property: event.active,
      order: event.direction,
    });
  }
}
