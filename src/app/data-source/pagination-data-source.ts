import { Sort } from "../model/page/sort";
import { Page } from "../model/page/page";
import { Observable, Subject, BehaviorSubject } from "rxjs";
import { DataSource } from "@angular/cdk/collections";
import { PaginatedEndpoint } from "../model/page/paginated-endpoint";
import { startWith, switchMap, share, map } from "rxjs/operators";

export class PaginatedDataSource<T> extends DataSource<T> {
  private pageNumber = new BehaviorSubject<number>(0);
  private sort = new Subject<Sort<T>>();

  public page: Observable<Page<T>>;

  public get currentPageNumber(): number {
    return this.pageNumber.value;
  }

  constructor(endpoint: PaginatedEndpoint<T>, initialSort: Sort<T>, size = 3) {
    super();
    this.page = this.sort.pipe(
      startWith(initialSort),
      switchMap((sort) =>
        this.pageNumber.pipe(
          startWith(0),
          switchMap((page) => endpoint({ page, sort, size })),
          share()
        )
      )
    );
  }

  sortBy(sort: Sort<T>): void {
    this.sort.next(sort);
  }

  fetch(page: number): void {
    this.pageNumber.next(page);
  }

  connect(): Observable<T[]> {
    const rows = [];

    return this.page.pipe(
      map((newPage) => {
        rows.splice(0, rows.length);
        newPage.content.forEach((element) => {
          rows.push(element, { detailRow: true, element });
        });
        return rows;
      })
    );
  }

  disconnect(): void {}
}
