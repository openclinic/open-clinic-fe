import { Component, OnInit, Input } from "@angular/core";
import { Visit } from "../model/visit";

@Component({
  selector: "app-visit-details",
  templateUrl: "./visit-details.component.html",
  styleUrls: ["./visit-details.component.scss"],
})
export class VisitDetailsComponent implements OnInit {
  @Input()
  visit: Visit;

  constructor() {}

  ngOnInit() {}
}
