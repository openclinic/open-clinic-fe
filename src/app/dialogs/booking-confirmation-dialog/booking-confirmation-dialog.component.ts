import { Component, Inject } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from "@angular/material/dialog";
import { VisitService } from "src/app/services/visit.service";
import { NewVisit } from "src/app/model/new-visit";
import { Router } from "@angular/router";
import { BookingErrorDialogComponent } from "../booking-error-dialog/booking-error-dialog.component";

@Component({
  selector: "app-booking-confirmation-dialog",
  templateUrl: "./booking-confirmation-dialog.component.html",
  styleUrls: ["./booking-confirmation-dialog.component.scss"],
})
export class BookingConfirmationDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<BookingConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NewVisit,
    private visitService: VisitService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  book() {
    this.visitService.book(this.data).subscribe(
      () => {
        this.router.navigate(["/patient-portal/visits"]);
        this.onNoClick();
      },
      (err) => {
        this.dialog.open(BookingErrorDialogComponent, {
          width: "500px",
          height: "400px",
          data: null,
        });
        this.onNoClick();
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
