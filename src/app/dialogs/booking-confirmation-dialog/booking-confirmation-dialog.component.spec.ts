import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingConfirmationDialogComponent } from './booking-confirmation-dialog.component';

describe('BookingConfirmationDialogComponent', () => {
  let component: BookingConfirmationDialogComponent;
  let fixture: ComponentFixture<BookingConfirmationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingConfirmationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
