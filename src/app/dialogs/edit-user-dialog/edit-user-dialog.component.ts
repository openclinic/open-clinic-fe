import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { noWhitespaceValidator } from "src/app/authentication/model/no-whitespace.validator";
import { User } from "src/app/authentication/model/user";
import { UserService } from "src/app/services/user.service";
import { UserUpdate } from "src/app/model/user-update";
import { Patient } from "src/app/model/patient";
import { AuthenticationService } from "src/app/authentication/authentication.service";

@Component({
  selector: "app-edit-user-dialog",
  templateUrl: "./edit-user-dialog.component.html",
  styleUrls: ["./edit-user-dialog.component.scss"],
})
export class EditUserDialogComponent implements OnInit {
  editForm: FormGroup;
  hide = true;
  error: string = null;

  constructor(
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.editForm = new FormGroup({
      email: new FormControl(this.data.email, [
        Validators.required,
        Validators.minLength(5),
        Validators.email,
        noWhitespaceValidator,
      ]),
      phoneNumber: new FormControl(this.data.phoneNumber, [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
        noWhitespaceValidator,
      ]),
      password: new FormControl("", [
        Validators.minLength(8),
        noWhitespaceValidator,
      ]),
    });
  }

  updateProfile() {
    if (this.editForm.invalid) {
      return;
    }
    const email = this.editForm.get("email").value;
    const password = this.editForm.get("password").value;
    const phoneNumber = this.editForm.get("phoneNumber").value;
    const userUpdate = new UserUpdate(email, password, phoneNumber);

    this.userService.update(this.data.uuid, userUpdate).subscribe(
      (user) => {
        this.authenticationService.sendUser(user);
        window.location.reload();
      },
      (errorResponse) => {
        this.error = errorResponse;
      }
    );
  }

  getErrorMessage(control: AbstractControl) {
    if (control.hasError("whitespace")) {
      return "Remove whitespaces ";
    } else if (control.hasError("email")) {
      return "Enter an email";
    } else {
      return "Length incorrect";
    }
  }
}
