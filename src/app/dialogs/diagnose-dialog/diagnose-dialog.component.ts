import { Component, OnInit, Inject } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { NewDiagnosis } from "src/app/model/newDiagnosis";
import { DiagnosisService } from "src/app/services/diagnosis.service";

@Component({
  selector: "app-diagnose-dialog",
  templateUrl: "./diagnose-dialog.component.html",
  styleUrls: ["./diagnose-dialog.component.scss"],
})
export class DiagnoseDialogComponent implements OnInit {
  diagnoseForm: FormGroup;
  error: string = null;

  constructor(
    public dialogRef: MatDialogRef<DiagnoseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private diagnosisService: DiagnosisService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.diagnoseForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(3)]),
      description: new FormControl("", [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(50),
      ]),
    });
  }

  createDiagnosis() {
    if (this.diagnoseForm.invalid) {
      return;
    }
    const name = this.diagnoseForm.get("name").value;
    const description = this.diagnoseForm.get("description").value;
    const patientUUID = this.data.patientUUID;
    const doctorUUID = this.data.doctorUUID;
    const visitUUID = this.data.visitUUID;

    const newDiagnosis: NewDiagnosis = new NewDiagnosis(
      name,
      description,
      patientUUID,
      doctorUUID,
      visitUUID
    );

    this.diagnosisService.create(newDiagnosis).subscribe(
      () => {
        window.location.reload();
      },
      (errorResponse) => {
        this.error = errorResponse;
      }
    );
  }

  getErrorMessage(control: AbstractControl) {
    if (control.hasError("whitespace")) {
      return "Remove whitespaces ";
    } else {
      return "Length incorrect";
    }
  }
}
