import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnoseDialogComponent } from './diagnose-dialog.component';

describe('DiagnoseDialogComponent', () => {
  let component: DiagnoseDialogComponent;
  let fixture: ComponentFixture<DiagnoseDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnoseDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnoseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
