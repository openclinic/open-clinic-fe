import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicDetailsDialogComponent } from './clinic-details-dialog.component';

describe('ClinicDetailsDialogComponent', () => {
  let component: ClinicDetailsDialogComponent;
  let fixture: ComponentFixture<ClinicDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
