import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Clinic } from "../../model/clinic";

@Component({
  selector: "app-clinic-details-dialog",
  templateUrl: "./clinic-details-dialog.component.html",
  styleUrls: ["./clinic-details-dialog.component.scss"],
})
export class ClinicDetailsDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ClinicDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Clinic
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
