import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescribingDialogComponent } from './prescribing-dialog.component';

describe('PrescribingDialogComponent', () => {
  let component: PrescribingDialogComponent;
  let fixture: ComponentFixture<PrescribingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrescribingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescribingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
