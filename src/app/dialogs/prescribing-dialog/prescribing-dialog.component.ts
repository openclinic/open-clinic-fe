import { Component, OnInit, Inject } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { NewPrescription } from "src/app/model/new-prescription";
import { PrescriptionService } from "src/app/services/prescription.service";

@Component({
  selector: "app-prescribing-dialog",
  templateUrl: "./prescribing-dialog.component.html",
  styleUrls: ["./prescribing-dialog.component.scss"],
})
export class PrescribingDialogComponent implements OnInit {
  prescriptionForm: FormGroup;
  error: string = null;

  constructor(
    public dialogRef: MatDialogRef<PrescribingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private prescriptionService: PrescriptionService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.prescriptionForm = new FormGroup({
      medicineName: new FormControl("", [
        Validators.required,
        Validators.minLength(3),
      ]),
      description: new FormControl("", [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(50),
      ]),
    });
  }

  createPrescription() {
    if (this.prescriptionForm.invalid) {
      return;
    }
    const medicineName = this.prescriptionForm.get("medicineName").value;
    const description = this.prescriptionForm.get("description").value;
    const patientUUID = this.data.patientUUID;
    const doctorUUID = this.data.doctorUUID;

    const newPrescription: NewPrescription = new NewPrescription(
      medicineName,
      description,
      patientUUID,
      doctorUUID
    );

    this.prescriptionService.create(newPrescription).subscribe(
      () => {
        window.location.reload();
      },
      (errorResponse) => {
        this.error = errorResponse;
      }
    );
  }

  getErrorMessage(control: AbstractControl) {
    if (control.hasError("whitespace")) {
      return "Remove whitespaces ";
    } else {
      return "Length incorrect";
    }
  }
}
