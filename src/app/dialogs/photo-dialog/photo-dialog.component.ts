import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { User } from "src/app/authentication/model/user";
import { AuthenticationService } from "src/app/authentication/authentication.service";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-photo-dialog",
  templateUrl: "./photo-dialog.component.html",
  styleUrls: ["./photo-dialog.component.scss"],
})
export class PhotoDialogComponent implements OnInit {
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  user: User;

  constructor(
    public dialogRef: MatDialogRef<PhotoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
  }

  public onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    const uploadImageData = new FormData();
    uploadImageData.append(
      "imageFile",
      this.selectedFile,
      this.selectedFile.name
    );
    this.userService
      .uploadPhoto(this.user.uuid, uploadImageData)
      .subscribe(() => {
        window.location.reload();
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
