import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Patient } from "src/app/model/patient";

@Component({
  selector: "app-patient-dialog",
  templateUrl: "./patient-dialog.component.html",
  styleUrls: ["./patient-dialog.component.scss"],
})
export class PatientDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<PatientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Patient
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
