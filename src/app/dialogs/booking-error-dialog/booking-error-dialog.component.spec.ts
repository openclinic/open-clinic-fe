import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingErrorDialogComponent } from './booking-error-dialog.component';

describe('BookingErrorDialogComponent', () => {
  let component: BookingErrorDialogComponent;
  let fixture: ComponentFixture<BookingErrorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingErrorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingErrorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
