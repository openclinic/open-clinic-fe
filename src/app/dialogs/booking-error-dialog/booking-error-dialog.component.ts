import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { VisitService } from "src/app/services/visit.service";
import { NewVisit } from "src/app/model/new-visit";
import { Router } from "@angular/router";

@Component({
  selector: "app-booking-error-dialog",
  templateUrl: "./booking-error-dialog.component.html",
  styleUrls: ["./booking-error-dialog.component.scss"],
})
export class BookingErrorDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<BookingErrorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
