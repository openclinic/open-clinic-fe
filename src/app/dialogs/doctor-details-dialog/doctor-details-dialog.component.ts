import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Doctor } from "../../model/doctor";

@Component({
  selector: "app-doctor-details-dialog",
  templateUrl: "./doctor-details-dialog.component.html",
  styleUrls: ["./doctor-details-dialog.component.scss"],
})
export class DoctorDetailsDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DoctorDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Doctor
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
