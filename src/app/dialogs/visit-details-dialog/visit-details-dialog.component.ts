import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Visit } from "src/app/model/visit";

@Component({
  selector: "app-visit-details-dialog",
  templateUrl: "./visit-details-dialog.component.html",
  styleUrls: ["./visit-details-dialog.component.scss"],
})
export class VisitDetailsDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<VisitDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Visit
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
