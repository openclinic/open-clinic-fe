import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/authentication/authentication.service";
import { ActivatedRoute } from "@angular/router";
import { PatientService } from "../services/patient.service";

@Component({
  selector: "app-prescription-page",
  templateUrl: "./prescription-page.component.html",
  styleUrls: ["./prescription-page.component.scss"],
})
export class PrescriptionPageComponent implements OnInit {
  user: any;
  isUser: any;
  isDoctor: any;

  constructor(
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private patientService: PatientService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.isUser = params.isUser;
      this.isDoctor = params.isDoctor;
      if (this.isUser == "true") {
        this.user = this.authenticationService.currentUserValue;
      } else {
        this.patientService
          .getByUUID(params.patientUUID)
          .subscribe((patient) => {
            this.user = patient;
          });
      }
    });
  }
}
