import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { SigninFormComponent } from "./authentication/signin-form/signin-form.component";
import { AuthGuard } from "./authentication/interceptors/auth.guard";
import { Role } from "./authentication/model/role";
import { PatientHeaderComponent } from "./patient-portal/patient-header/patient-header.component";
import { DoctorsPageComponent } from "./patient-portal/doctors-page/doctors-page.component";
import { PatientMainPageComponent } from "./patient-portal/patient-main-page/patient-main-page.component";
import { DoctorHeaderComponent } from "./doctors-portal/doctor-header/doctor-header.component";
import { VisitsPageComponent } from "./patient-portal/visits-page/visits-page.component";
import { VisitsBookingPageComponent } from "./patient-portal/visits-booking-page/visits-booking-page.component";
import { DocumentationPageComponent } from "./patient-portal/documentation-page/documentation-page.component";
import { PrescriptionPageComponent } from "./prescription-page/prescription-page.component";
import { DoctorVisitsComponent } from "./doctors-portal/doctor-visits/doctor-visits.component";
import { PatientsPageComponent } from "./doctors-portal/patients-page/patients-page.component";
import { DoctorProfilePageComponent } from "./doctors-portal/doctor-profile-page/doctor-profile-page.component";

const routes: Routes = [
  { path: "", component: LandingPageComponent },
  { path: "login", component: SigninFormComponent },
  {
    path: "patient-portal",
    component: PatientHeaderComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Patient] },
    children: [
      {
        path: "main-page",
        component: PatientMainPageComponent,
      },
      {
        path: "doctors",
        component: DoctorsPageComponent,
      },
      {
        path: "visits",
        component: VisitsPageComponent,
      },
      {
        path: "book-visit",
        component: VisitsBookingPageComponent,
      },
      {
        path: "documentation",
        component: DocumentationPageComponent,
      },
      {
        path: "prescriptions",
        component: PrescriptionPageComponent,
      },
    ],
  },
  {
    path: "doctor-portal",
    component: DoctorHeaderComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] },
    children: [
      {
        path: "visits",
        component: DoctorVisitsComponent,
      },
      {
        path: "patient-documentation",
        component: DocumentationPageComponent,
      },
      {
        path: "prescriptions",
        component: PrescriptionPageComponent,
      },
      {
        path: "patients",
        component: PatientsPageComponent,
      },
      {
        path: "profile",
        component: DoctorProfilePageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
