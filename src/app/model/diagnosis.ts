export interface Diagnosis {
  uuid: string;
  name: string;
  description: string;
  date: Date;
  doctorUUID: string;
  visitUUID: string;
}
