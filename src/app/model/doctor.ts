import { Specialization } from "./specialization";

export interface Doctor {
  uuid: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  specialization: Specialization;
  description: string;
  photo: any;
}
