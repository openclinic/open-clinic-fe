export interface PatientShort {
  uuid: string;
  firstName: string;
  lastName: string;
}
