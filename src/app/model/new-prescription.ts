export class NewPrescription {
  constructor(
    public medicineName: string,
    public description: string,
    public patientUUID: string,
    public doctorUUID: string
  ) {}
}
