import { VisitStatus } from "./visitStatus";
import { DoctorShort } from "./doctor-short";
import { ClinicShort } from "./clinic-short";
import { PatientShort } from "./patient-short";

export interface Visit {
  uuid: string;
  startDate: Date;
  endDate: Date;
  description: VisitStatus;
  price: number;
  type: string;
  doctor: DoctorShort;
  patient: PatientShort;
  clinic: ClinicShort;
  visitStatus: VisitStatus;
}
