import { PageRequest } from "./page-request";
import { Page } from "./page";
import { Observable } from "rxjs";

export type PaginatedEndpoint<T> = (req: PageRequest<T>) => Observable<Page<T>>;
