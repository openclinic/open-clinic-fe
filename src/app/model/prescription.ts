export interface Prescription {
  uuid: string;
  code: string;
  dateOfIssuing: Date;
  dateOfExpiration: Date;
  medicineName: string;
  description: string;
  doctorName: string;
  patientName: string;
}
