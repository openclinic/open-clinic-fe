export enum VisitStatus {
  Booked = "BOOKED",
  Canceled = "CANCELED",
  Finished = "FINISHED",
}
