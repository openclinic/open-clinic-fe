export interface Patient {
  uuid: string;
  firstName: string;
  lastName: string;
  email: string;
  dateOfJoining: Date;
  sex: string;
  identityNumber: string;
  phoneNumber: string;
  photo: any;
}
