import { Address } from "./Address";

export interface Clinic {
  uuid: string;
  name: string;
  address: Address;
  phoneNumber: string;
  photo: any;
}
