export interface ClinicShort {
  uuid: string;
  name: string;
}
