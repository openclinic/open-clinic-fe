export interface DoctorShort {
  uuid: string;
  firstName: string;
  lastName: string;
}
