export class UserUpdate {
  constructor(
    public email: string,
    public password: string,
    public phoneNumber: string
  ) {}
}
