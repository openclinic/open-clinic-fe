export class NewDiagnosis {
  constructor(
    public name: string,
    public description: string,
    public patientUUID: string,
    public doctorUUID: string,
    public visitUUID: string
  ) {}
}
