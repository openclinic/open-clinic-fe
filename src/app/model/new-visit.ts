export class NewVisit {
  constructor(
    public patientUUID: string,
    public doctorUUID: string,
    public clinicUUID: string,
    public startDate: string
  ) {}
}
