import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { Doctor } from "../model/doctor";
import { map, catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class DoctorService {
  baseUrl = `${environment.baseUrl}doctors`;

  constructor(private http: HttpClient) {}

  getLeadingDoctor(patientUuid: string): Observable<Doctor> {
    const searchParams = new HttpParams().append(
      `patientUUID`,
      `${patientUuid}`
    );
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http.get<Doctor>(`${this.baseUrl}`, httpOptions).pipe(
      map((doctor) => doctor),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getByUUID(uuid: string): Observable<Doctor> {
    return this.http.get<Doctor>(`${this.baseUrl}` + "/" + uuid).pipe(
      map((doctor) => doctor),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getSpecializations(): Observable<Array<String>> {
    return this.http
      .get<Array<String>>(`${this.baseUrl}` + "/specializations")
      .pipe(
        map((specializations) => specializations),
        catchError((errorRes) => {
          return throwError(errorRes);
        })
      );
  }

  getAll(): Observable<Array<Doctor>> {
    return this.http.get<Array<Doctor>>(`${this.baseUrl}`).pipe(
      map((doctors) => doctors),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getAllBySpecialization(specialization: string): Observable<Array<Doctor>> {
    const searchParams = new HttpParams().append(
      `specialization`,
      `${specialization}`
    );
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http.get<Array<Doctor>>(`${this.baseUrl}`, httpOptions).pipe(
      map((doctors) => {
        return doctors;
      }),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getAvailableDoctors(
    date: string,
    specialization: string,
    clinicUuid: string
  ): Observable<Array<Doctor>> {
    const finalDate = date.concat(":00").replace("T", " ");
    const searchParams = new HttpParams()
      .append(`date`, `${finalDate}`)
      .append(`specialization`, `${specialization}`)
      .append(`clinicUUID`, `${clinicUuid}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http.get<Array<Doctor>>(`${this.baseUrl}`, httpOptions).pipe(
      map((doctors) => doctors),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }
}
