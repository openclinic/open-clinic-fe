import { Injectable } from "@angular/core";
import { PageRequest } from "../model/page/page-request";
import { Page } from "../model/page/page";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { Visit } from "../model/visit";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { NewVisit } from "../model/new-visit";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class VisitService {
  baseUrl = `${environment.baseUrl}visits`;
  doctorUrl = `${environment.baseUrl}doctor/visits`;

  constructor(private http: HttpClient) {}

  getByUUID(uuid: string): Observable<Visit> {
    return this.http.get<Visit>(`${this.baseUrl}` + "/" + uuid).pipe(
      map((visit) => visit),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getUpcoming(
    request: PageRequest<Visit>,
    patientUuid: string,
    status: string
  ): Observable<Page<Visit>> {
    const searchParams = new HttpParams()
      .append(`pageNumber`, `${request.page}`)
      .append(`pageSize`, `${request.size}`)
      .append(`sortField`, `${request.sort.property}`)
      .append(`direction`, `${request.sort.order.toUpperCase()}`)
      .append(`patientUUID`, `${patientUuid}`)
      .append("status", `${status}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http.get<Page<Visit>>(`${this.baseUrl}`, httpOptions).pipe(
      map((visit) => {
        return visit;
      }),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getAllForPatient(
    request: PageRequest<Visit>,
    patientUuid: string
  ): Observable<Page<Visit>> {
    const searchParams = new HttpParams()
      .append(`pageNumber`, `${request.page}`)
      .append(`pageSize`, `${request.size}`)
      .append(`sortField`, `${request.sort.property}`)
      .append(`direction`, `${request.sort.order.toUpperCase()}`)
      .append(`patientUUID`, `${patientUuid}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http
      .get<Page<Visit>>(`${this.baseUrl}` + "/all", httpOptions)
      .pipe(
        map((visits) => {
          return visits;
        }),
        catchError((errorRes) => {
          return throwError(errorRes);
        })
      );
  }

  getAllForDoctor(
    request: PageRequest<Visit>,
    doctorUuid: string
  ): Observable<Page<Visit>> {
    const searchParams = new HttpParams()
      .append(`pageNumber`, `${request.page}`)
      .append(`pageSize`, `${request.size}`)
      .append(`sortField`, `${request.sort.property}`)
      .append(`direction`, `${request.sort.order.toUpperCase()}`)
      .append(`doctorUUID`, `${doctorUuid}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http.get<Page<Visit>>(`${this.doctorUrl}`, httpOptions).pipe(
      map((visits) => {
        return visits;
      }),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  updateStatus(uuid: string, status: string): Observable<Visit> {
    return this.http
      .patch<Visit>(`${this.baseUrl}` + "/" + uuid + "?status=" + status, null)
      .pipe(
        map((visit) => visit),
        catchError((errorRes) => {
          return throwError(errorRes);
        })
      );
  }

  book(visit: NewVisit): Observable<Visit> {
    return this.http.post<Visit>(`${this.baseUrl}`, visit).pipe(
      map((visit) => visit),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }
}
