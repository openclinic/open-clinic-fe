import { TestBed } from "@angular/core/testing";

import { DoctorTransferService } from "../services/doctor-transfer.service";

describe("DoctorTransferService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: DoctorTransferService = TestBed.get(DoctorTransferService);
    expect(service).toBeTruthy();
  });
});
