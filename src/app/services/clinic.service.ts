import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
import { Clinic } from "../model/clinic";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class ClinicService {
  baseUrl = `${environment.baseUrl}clinics`;

  constructor(private http: HttpClient) {}

  getByUUID(Uuid: string): Observable<Clinic> {
    return this.http.get<Clinic>(`${this.baseUrl}` + "/" + Uuid).pipe(
      map((clinic) => clinic),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getAll(): Observable<Array<Clinic>> {
    return this.http.get<Array<Clinic>>(`${this.baseUrl}`).pipe(
      map((clinics) => clinics),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }
}
