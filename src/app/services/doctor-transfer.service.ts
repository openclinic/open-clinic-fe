import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Doctor } from "../model/doctor";

@Injectable({
  providedIn: "root",
})
export class DoctorTransferService {
  private doctorSubject = new Subject<Doctor>();

  constructor() {}

  sendDoctor(doctor: Doctor) {
    this.doctorSubject.next(doctor);
  }

  subscribe(request: any) {
    return this.doctorSubject.subscribe(request);
  }
}
