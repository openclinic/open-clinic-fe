import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { PageRequest } from "../model/page/page-request";
import { Prescription } from "../model/prescription";
import { Observable, throwError } from "rxjs";
import { Page } from "../model/page/page";
import { map, catchError } from "rxjs/operators";
import { NewPrescription } from "../model/new-prescription";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class PrescriptionService {
  patientUrl = `${environment.baseUrl}patient/prescriptions`;
  doctorUrl = `${environment.baseUrl}doctor/prescriptions`;

  constructor(private http: HttpClient) {}

  getAllByPatientUUID(
    request: PageRequest<Prescription>,
    patientUuid: string
  ): Observable<Page<Prescription>> {
    const searchParams = new HttpParams()
      .append(`pageNumber`, `${request.page}`)
      .append(`pageSize`, `${request.size}`)
      .append(`sortField`, `${request.sort.property}`)
      .append(`direction`, `${request.sort.order.toUpperCase()}`)
      .append(`patientUUID`, `${patientUuid}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http
      .get<Page<Prescription>>(`${this.patientUrl}`, httpOptions)
      .pipe(
        map((diagnoses) => diagnoses),
        catchError((errorRes) => {
          return throwError(errorRes);
        })
      );
  }

  getAllByDoctorUUID(
    request: PageRequest<Prescription>,
    doctorUuid: string
  ): Observable<Page<Prescription>> {
    const searchParams = new HttpParams()
      .append(`pageNumber`, `${request.page}`)
      .append(`pageSize`, `${request.size}`)
      .append(`sortField`, `${request.sort.property}`)
      .append(`direction`, `${request.sort.order.toUpperCase()}`)
      .append(`doctorUUID`, `${doctorUuid}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http
      .get<Page<Prescription>>(`${this.doctorUrl}`, httpOptions)
      .pipe(
        map((diagnoses) => diagnoses),
        catchError((errorRes) => {
          return throwError(errorRes);
        })
      );
  }

  create(prescription: NewPrescription): Observable<Prescription> {
    return this.http.post<Prescription>(`${this.doctorUrl}`, prescription).pipe(
      map((prescription) => prescription),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }
}
