import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { Diagnosis } from "../model/diagnosis";
import { PageRequest } from "../model/page/page-request";
import { Page } from "../model/page/page";
import { map, catchError } from "rxjs/operators";
import { NewDiagnosis } from "../model/newDiagnosis";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class DiagnosisService {
  baseUrl = `${environment.baseUrl}diagnoses`;

  constructor(private http: HttpClient) {}

  getAllByPatientUUID(
    request: PageRequest<Diagnosis>,
    patientUuid: string
  ): Observable<Page<Diagnosis>> {
    const searchParams = new HttpParams()
      .append(`pageNumber`, `${request.page}`)
      .append(`pageSize`, `${request.size}`)
      .append(`sortField`, `${request.sort.property}`)
      .append(`direction`, `${request.sort.order.toUpperCase()}`)
      .append(`patientUUID`, `${patientUuid}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http.get<Page<Diagnosis>>(`${this.baseUrl}`, httpOptions).pipe(
      map((diagnoses) => diagnoses),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  create(diagnosis: NewDiagnosis): Observable<Diagnosis> {
    return this.http.post<Diagnosis>(`${this.baseUrl}`, diagnosis).pipe(
      map((diagnosis) => diagnosis),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }
}
