import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { Patient } from "../model/patient";
import { map, catchError } from "rxjs/operators";
import { PageRequest } from "../model/page/page-request";
import { Page } from "../model/page/page";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class PatientService {
  baseUrl = `${environment.baseUrl}patients`;

  constructor(private http: HttpClient) {}

  getByUUID(patientUuid: string): Observable<Patient> {
    return this.http.get<Patient>(`${this.baseUrl}` + "/" + patientUuid).pipe(
      map((patient) => {
        return patient;
      }),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }

  getPatientsForDoctor(
    request: PageRequest<Patient>,
    doctorUuid: string
  ): Observable<Page<Patient>> {
    const searchParams = new HttpParams()
      .append(`pageNumber`, `${request.page}`)
      .append(`pageSize`, `${request.size}`)
      .append(`sortField`, `${request.sort.property}`)
      .append(`direction`, `${request.sort.order.toUpperCase()}`)
      .append(`doctorUUID`, `${doctorUuid}`);
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      params: searchParams,
    };
    return this.http.get<Page<Patient>>(`${this.baseUrl}`, httpOptions).pipe(
      map((patients) => {
        return patients;
      }),
      catchError((errorRes) => {
        return throwError(errorRes);
      })
    );
  }
}
