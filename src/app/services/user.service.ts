import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserUpdate } from "../model/user-update";
import { User } from "../authentication/model/user";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class UserService {
  baseUrl = `${environment.baseUrl}`;

  constructor(private httpClient: HttpClient) {}

  uploadPhoto(userUUID: string, uploadImageData: any): Observable<any> {
    return this.httpClient.patch(
      `${this.baseUrl}` + userUUID + "/photo",
      uploadImageData
    );
  }

  update(userUUID: string, userUpdate: UserUpdate): Observable<User> {
    return this.httpClient.patch<User>(
      `${this.baseUrl}` + userUUID,
      userUpdate
    );
  }
}
