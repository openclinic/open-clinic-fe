import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material/list";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCardModule } from "@angular/material/card";
import { MatTableModule } from "@angular/material/table";
import { MatDialogModule } from "@angular/material/dialog";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule } from "@angular/material/sort";
import { MatSelectModule } from "@angular/material/select";

import { AppComponent } from "./app.component";
import { SigninFormComponent } from "./authentication/signin-form/signin-form.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { LoadingSpinnerComponent } from "./loading-spinner/loading-spinner.component";
import { PatientMainPageComponent } from "./patient-portal/patient-main-page/patient-main-page.component";
import { AuthInterceptor } from "./authentication/interceptors/auth.interceptor";
import { JwtInterceptor } from "./authentication/interceptors/jwt.interceptor";
import { DoctorMainPageComponent } from "./doctors-portal/doctor-main-page/doctor-main-page.component";
import { PatientHeaderComponent } from "./patient-portal/patient-header/patient-header.component";
import { DoctorDetailsDialogComponent } from "./dialogs/doctor-details-dialog/doctor-details-dialog.component";
import { DoctorDetailsComponent } from "./patient-portal/doctor-details/doctor-details.component";
import { ClinicDetailsDialogComponent } from "./dialogs/clinic-details-dialog/clinic-details-dialog.component";
import { ClinicDetailsComponent } from "./patient-portal/clinic-details/clinic-details.component";
import { VisitsTableComponent } from "./patient-portal/visits-table/visits-table.component";
import { DoctorsPageComponent } from "./patient-portal/doctors-page/doctors-page.component";
import { DoctorHeaderComponent } from "./doctors-portal/doctor-header/doctor-header.component";
import { VisitsPageComponent } from "./patient-portal/visits-page/visits-page.component";
import { VisitsBookingPageComponent } from "./patient-portal/visits-booking-page/visits-booking-page.component";
import { DoctorsDeckComponent } from "./patient-portal/doctors-deck/doctors-deck.component";
import { BookingConfirmationDialogComponent } from "./dialogs/booking-confirmation-dialog/booking-confirmation-dialog.component";
import { BookingErrorDialogComponent } from "./dialogs/booking-error-dialog/booking-error-dialog.component";
import { DocumentationPageComponent } from "./patient-portal/documentation-page/documentation-page.component";
import { DiagnosesTableComponent } from "./patient-portal/documentation-page/diagnoses-table/diagnoses-table.component";
import { VisitDetailsDialogComponent } from "./dialogs/visit-details-dialog/visit-details-dialog.component";
import { VisitDetailsComponent } from "./visit-details/visit-details.component";
import { PrescriptionsTableComponent } from "./prescriptions-table/prescriptions-table.component";
import { PrescriptionPageComponent } from "./prescription-page/prescription-page.component";
import { EditUserDialogComponent } from "./dialogs/edit-user-dialog/edit-user-dialog.component";
import { PhotoDialogComponent } from "../app/dialogs/photo-dialog/photo-dialog.component";
import { DoctorVisitsComponent } from "./doctors-portal/doctor-visits/doctor-visits.component";
import { PatientDialogComponent } from "./dialogs/patient-dialog/patient-dialog.component";
import { PatientDetailsComponent } from "./doctors-portal/patient-details/patient-details.component";
import { PatientsTableComponent } from "./doctors-portal/patients-table/patients-table.component";
import { PatientsPageComponent } from "./doctors-portal/patients-page/patients-page.component";
import { PrescribingDialogComponent } from "./dialogs/prescribing-dialog/prescribing-dialog.component";
import { DiagnoseDialogComponent } from "./dialogs/diagnose-dialog/diagnose-dialog.component";
import { DoctorProfilePageComponent } from './doctors-portal/doctor-profile-page/doctor-profile-page.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninFormComponent,
    LandingPageComponent,
    LoadingSpinnerComponent,
    PatientMainPageComponent,
    DoctorMainPageComponent,
    PatientHeaderComponent,
    DoctorDetailsDialogComponent,
    DoctorDetailsComponent,
    ClinicDetailsDialogComponent,
    ClinicDetailsComponent,
    VisitsTableComponent,
    DoctorsPageComponent,
    DoctorHeaderComponent,
    VisitsPageComponent,
    VisitsBookingPageComponent,
    DoctorsDeckComponent,
    BookingConfirmationDialogComponent,
    BookingErrorDialogComponent,
    DocumentationPageComponent,
    DiagnosesTableComponent,
    VisitDetailsDialogComponent,
    VisitDetailsComponent,
    PrescriptionsTableComponent,
    PrescriptionPageComponent,
    EditUserDialogComponent,
    PhotoDialogComponent,
    DoctorVisitsComponent,
    PatientDialogComponent,
    PatientDetailsComponent,
    PatientsTableComponent,
    PatientsPageComponent,
    PrescribingDialogComponent,
    DiagnoseDialogComponent,
    DoctorProfilePageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatCardModule,
    MatTableModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DoctorDetailsDialogComponent,
    ClinicDetailsDialogComponent,
    BookingConfirmationDialogComponent,
    BookingErrorDialogComponent,
    VisitDetailsDialogComponent,
    EditUserDialogComponent,
    PhotoDialogComponent,
    PatientDialogComponent,
    PrescribingDialogComponent,
    DiagnoseDialogComponent,
  ],
})
export class AppModule {}
