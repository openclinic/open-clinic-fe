import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
  NgForm,
} from "@angular/forms";
import { noWhitespaceValidator } from "../model/no-whitespace.validator";
import { AuthenticationService } from "../authentication.service";
import { Router } from "@angular/router";
import { User } from "../model/user";
import { Role } from "../model/role";

@Component({
  selector: "app-signin-form",
  templateUrl: "./signin-form.component.html",
  styleUrls: ["./signin-form.component.scss"],
})
export class SigninFormComponent implements OnInit {
  @ViewChild("f", { static: false })
  resetForm: NgForm;

  signInForm: FormGroup;

  hide = true;
  isLoading = false;
  error: string = null;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authenticationService.logout();
    this.signInForm = new FormGroup({
      email: new FormControl("", [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(128),
        Validators.email,
        noWhitespaceValidator,
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(128),
        noWhitespaceValidator,
      ]),
    });
  }

  onSubmit() {
    if (this.signInForm.invalid) {
      return;
    }
    const email = this.signInForm.get("email").value;
    const password = this.signInForm.get("password").value;

    this.isLoading = true;

    this.authenticationService.login(email, password).subscribe(
      () => {
        this.isLoading = false;
        this.error = null;
        this.navigateToMainPage();
      },
      (errorResponse) => {
        this.isLoading = false;
        this.error = errorResponse;
      }
    );

    this.resetForm.resetForm();
  }

  navigateToMainPage() {
    const user: User = this.authenticationService.currentUserValue;
    if (this.isDoctor(user)) {
      this.router.navigate(["/doctor-portal/visits"]);
    } else {
      this.router.navigate(["/patient-portal/main-page"]);
    }
  }

  isDoctor(user: User) {
    return user.role == Role.Doctor;
  }

  getErrorMessage(control: AbstractControl) {
    if (control.hasError("required")) {
      return "Enter a value";
    } else if (control.hasError("whitespace")) {
      return "Remove whitespaces ";
    } else if (control.hasError("email")) {
      return "Enter an email";
    } else {
      return "Length incorrect";
    }
  }
}
