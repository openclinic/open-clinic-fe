import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const isAuth = localStorage.getItem("token");
    if (isAuth) {
      request = request.clone({
        setHeaders: {
          Authorization: `${isAuth}`,
        },
      });
    }
    return next.handle(request);
  }
}
