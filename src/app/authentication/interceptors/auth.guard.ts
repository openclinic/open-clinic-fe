import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { AuthenticationService } from "../authentication.service";
import { User } from "../model/user";
@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
  constructor(private authenticationService: AuthenticationService) {}

  isUserAllowed(route, role) {
    return route.data.roles.indexOf(role) > -1;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const user: User = this.authenticationService.currentUserValue;

    if (user) {
      if (
        (route.data.roles && !this.isUserAllowed(route, user.role)) ||
        this.authenticationService.isTokenExpired(localStorage.getItem("token"))
      ) {
        this.authenticationService.logout();
        return false;
      } else {
        return true;
      }
    } else {
      this.authenticationService.logout();
      return false;
    }
  }
}
