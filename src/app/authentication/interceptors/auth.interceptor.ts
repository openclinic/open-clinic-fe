import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpUserEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
import { tap } from "rxjs/operators";
import * as jwt_decode from "jwt-decode";
import { Observable } from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  error: any;

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<
    | HttpSentEvent
    | HttpHeaderResponse
    | HttpProgressEvent
    | HttpResponse<any>
    | HttpUserEvent<any>
  > {
    return next.handle(req).pipe(
      tap(
        (res: any) => {
          console.log("here");
          console.log(res);
          if (res.headers && !localStorage.getItem("currentUser")) {
            console.log(res.headers.get("Authorization"));
            const token = res.headers.get("Authorization");
            const decoded = jwt_decode(token);
            localStorage.setItem("currentUser", JSON.stringify(decoded));
            localStorage.setItem("token", token);
          }
        },
        (err: any) => {
          console.log("intercetpor");
          if (err instanceof HttpErrorResponse) {
            this.error = err;
          }
        }
      )
    );
  }
}
