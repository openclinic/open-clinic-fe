import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, throwError, Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { User } from "./model/user";
import * as jwt_decode from "jwt-decode";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  baseUrl = `${environment.baseUrl}login`;

  private userSubject = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient, private router: Router) {}

  login(email: string, password: string): Observable<any> {
    return this.http
      .post<any>(`${this.baseUrl}`, {
        email: email,
        password: password,
      })
      .pipe(
        map((response) => {
          console.log("here");
          const user: User = this.getUserFromLocalStorage();
          this.userSubject.next(user);
          return response;
        }),
        catchError((error) => {
          console.log(error);
          return throwError("Email or password invalid");
        })
      );
  }

  logout() {
    this.userSubject.next(null);
    this.router.navigate(["/login"]);
    localStorage.removeItem("currentUser");
    localStorage.removeItem("token");
  }

  sendUser(user: User) {
    this.userSubject.next(user);
  }

  autoLogin() {
    const user: User = this.getUserFromLocalStorage();
    const token: string = localStorage.getItem("token");
    if (!user) {
      return;
    }

    if (token) {
      this.userSubject.next(user);
    }
  }

  public get currentUserValue(): User {
    return this.userSubject.value;
  }

  isTokenExpired(token): boolean {
    const date = this.getTokenExpirationDate(token);
    if (!date) {
      return false;
    }
    return !(date.valueOf() > new Date().valueOf());
  }

  private getUserFromLocalStorage(): User {
    const jwtData = JSON.parse(localStorage.getItem("currentUser"));
    if (!jwtData) {
      return null;
    }
    return new User(
      jwtData.uuid,
      jwtData.email,
      jwtData.role,
      jwtData.first_name,
      jwtData.last_name,
      jwtData.phone_number
    );
  }

  private getTokenExpirationDate(token): Date {
    if (token) {
      const decoded: any = jwt_decode(token);
      const date = new Date(0);
      date.setUTCSeconds(decoded.exp);
      return date;
    }
  }
}
