import { Role } from "./role";

export class User {
  constructor(
    public uuid: string,
    public email: string,
    public role: Role,
    public firstName: string,
    public lastName: string,
    public phoneNumber: string
  ) {}
}
