import { AbstractControl } from "@angular/forms";

export function noWhitespaceValidator(
  control: AbstractControl
): { [key: string]: any } | null {
  const isWhitespace = /\s/.test(control.value);
  const isValid = !isWhitespace;
  return isValid ? null : { whitespace: true };
}
