import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DoctorService } from "src/app/services/doctor.service";
import { ClinicService } from "src/app/services/clinic.service";
import { noWhitespaceValidator } from "src/app/authentication/model/no-whitespace.validator";
import { DoctorTransferService } from "src/app/services/doctor-transfer.service";
import { Doctor } from "src/app/model/doctor";
import { AuthenticationService } from "src/app/authentication/authentication.service";
import { User } from "src/app/authentication/model/user";
import { NewVisit } from "src/app/model/new-visit";
import { MatDialog } from "@angular/material/dialog";
import { BookingConfirmationDialogComponent } from "src/app/dialogs/booking-confirmation-dialog/booking-confirmation-dialog.component";

@Component({
  selector: "app-visits-booking-page",
  templateUrl: "./visits-booking-page.component.html",
  styleUrls: ["./visits-booking-page.component.scss"],
})
export class VisitsBookingPageComponent implements OnInit, OnDestroy {
  specializations = [];
  clinics = [];
  doctors = [];
  doctor: Doctor;
  user: User;
  bookingForm: FormGroup;
  subscription: any;
  isLoading: boolean;

  constructor(
    private doctorService: DoctorService,
    private clinicService: ClinicService,
    private doctorTransferService: DoctorTransferService,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.doctorService.getSpecializations().subscribe((specData) => {
      specData.forEach((specialization) =>
        this.specializations.push({
          value: specialization.toUpperCase(),
          viewValue: specialization,
        })
      );
    });
    this.clinicService.getAll().subscribe((clinicData) => {
      clinicData.forEach((clinic) =>
        this.clinics.push({
          value: clinic.uuid.toUpperCase(),
          viewValue: clinic.name,
        })
      );
    });
    this.subscription = this.doctorTransferService.subscribe((doctor) => {
      this.doctor = doctor;
      this.openBookingDialog();
    });
    this.user = this.authenticationService.currentUserValue;
    this.bookingForm = new FormGroup({
      specialization: new FormControl("", [
        Validators.required,
        Validators.minLength(2),
        noWhitespaceValidator,
      ]),
      date: new FormControl("", [
        Validators.required,
        Validators.minLength(2),
        noWhitespaceValidator,
      ]),
      clinic: new FormControl("", [
        Validators.required,
        Validators.minLength(2),
        noWhitespaceValidator,
      ]),
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAvailableDoctors() {
    this.isLoading = true;
    if (this.bookingForm.invalid) {
      this.isLoading = false;
      return;
    }
    const date = this.bookingForm.get("date").value;
    const specialization = this.bookingForm.get("specialization").value;
    const clinicUuid = this.bookingForm.get("clinic").value;
    this.doctorService
      .getAvailableDoctors(date, specialization, clinicUuid)
      .subscribe((doctors) => {
        this.doctors = doctors;
        this.isLoading = false;
      });
  }

  openBookingDialog(): void {
    const date = this.bookingForm.get("date").value;
    const clinicUuid = this.bookingForm.get("clinic").value;
    const visit = new NewVisit(
      this.user.uuid,
      this.doctor.uuid,
      clinicUuid,
      date
    );

    this.dialog.open(BookingConfirmationDialogComponent, {
      width: "500px",
      height: "300px",
      data: visit,
    });
  }
}
