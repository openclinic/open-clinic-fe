import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitsBookingPageComponent } from './visits-booking-page.component';

describe('VisitsBookingPageComponent', () => {
  let component: VisitsBookingPageComponent;
  let fixture: ComponentFixture<VisitsBookingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitsBookingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitsBookingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
