import { Component, OnInit, AfterViewInit } from "@angular/core";
import { AuthenticationService } from "../../authentication/authentication.service";
import { FocusMonitor } from "@angular/cdk/a11y";
import { PatientService } from "src/app/services/patient.service";

@Component({
  selector: "app-patient-header",
  templateUrl: "./patient-header.component.html",
  styleUrls: ["./patient-header.component.scss"],
})
export class PatientHeaderComponent implements OnInit, AfterViewInit {
  base64Data: any;
  retrievedImage: any;
  patient: any;

  constructor(
    private authenticationService: AuthenticationService,
    private patientService: PatientService,
    private _focusMonitor: FocusMonitor
  ) {}

  ngOnInit() {
    this.patientService
      .getByUUID(this.authenticationService.currentUserValue.uuid)
      .subscribe((user) => {
        this.patient = user;
        this.base64Data = user.photo;
        this.retrievedImage = "data:image/jpeg;base64," + this.base64Data;
      });
  }

  ngAfterViewInit() {
    this._focusMonitor.stopMonitoring(
      document.getElementById("sidenav_button")
    );
  }

  logout() {
    this.authenticationService.logout();
  }
}
