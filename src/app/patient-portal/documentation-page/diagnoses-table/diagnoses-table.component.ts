import { Component, OnInit, Input } from "@angular/core";
import { VisitService } from "../../../services/visit.service";
import { DoctorService } from "../../../services/doctor.service";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { MatDialog } from "@angular/material/dialog";
import { DoctorDetailsDialogComponent } from "../../../dialogs/doctor-details-dialog/doctor-details-dialog.component";
import { PaginatedDataSource } from "../../../data-source/pagination-data-source";
import { DiagnosisService } from "src/app/services/diagnosis.service";
import { Diagnosis } from "src/app/model/diagnosis";
import { VisitDetailsDialogComponent } from "src/app/dialogs/visit-details-dialog/visit-details-dialog.component";
import { Patient } from "src/app/model/patient";

@Component({
  selector: "app-diagnoses-table",
  templateUrl: "./diagnoses-table.component.html",
  styleUrls: ["./diagnoses-table.component.scss"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class DiagnosesTableComponent implements OnInit {
  @Input()
  patient: Patient;

  expandedDiagnosis: any;

  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty("detailRow");

  displayedColumns: string[] = ["Condition", "Description", "Date"];
  dataSource: any;
  endpoint: any;

  constructor(
    private diagnosesService: DiagnosisService,
    private visitService: VisitService,
    private doctorService: DoctorService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.endpoint = (request) =>
      this.diagnosesService.getAllByPatientUUID(request, this.patient.uuid);

    this.dataSource = new PaginatedDataSource<Diagnosis>(this.endpoint, {
      property: "date",
      order: "desc",
    });
  }

  openDoctorDialog(diagnosis: Diagnosis): void {
    this.doctorService.getByUUID(diagnosis.doctorUUID).subscribe((doctor) => {
      this.dialog.open(DoctorDetailsDialogComponent, {
        width: "500px",
        height: "550px",
        data: doctor,
      });
    });
  }

  openVisitDialog(diagnosis: Diagnosis): void {
    this.visitService.getByUUID(diagnosis.visitUUID).subscribe((visit) => {
      this.dialog.open(VisitDetailsDialogComponent, {
        width: "600px",
        height: "300px",
        data: visit,
      });
    });
  }

  onSort(event: any) {
    this.dataSource.sortBy({
      property: event.active,
      order: event.direction,
    });
  }
}
