import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/authentication/authentication.service";
import { Patient } from "src/app/model/patient";
import { PatientService } from "src/app/services/patient.service";
import { MatDialog } from "@angular/material/dialog";
import { EditUserDialogComponent } from "src/app/dialogs/edit-user-dialog/edit-user-dialog.component";
import { PhotoDialogComponent } from "src/app/dialogs/photo-dialog/photo-dialog.component";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-documentation-page",
  templateUrl: "./documentation-page.component.html",
  styleUrls: ["./documentation-page.component.scss"],
})
export class DocumentationPageComponent implements OnInit {
  patient: Patient;
  base64Data: any;
  retrievedImage: any;
  isEditable: any;

  constructor(
    private route: ActivatedRoute,
    private authentiationService: AuthenticationService,
    private patientService: PatientService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.isEditable = params.isEditable;
      let patientUUID = this.authentiationService.currentUserValue.uuid;
      if (this.isEditable !== "true") {
        patientUUID = params.patientUUID;
      }
      this.patientService.getByUUID(patientUUID).subscribe((patient) => {
        this.patient = patient;
        this.base64Data = this.patient.photo;
        this.retrievedImage = "data:image/jpeg;base64," + this.base64Data;
      });
    });
  }

  openEditDialog(): void {
    this.dialog.open(EditUserDialogComponent, {
      width: "500px",
      height: "550px",
      data: this.patient,
    });
  }

  openPhotoDialog(): void {
    this.dialog.open(PhotoDialogComponent, {
      width: "500px",
      height: "300px",
      data: this.patient,
    });
  }
}
