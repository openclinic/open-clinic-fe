import { Component, OnInit, Input } from "@angular/core";
import { Doctor } from "../../model/doctor";
import { DoctorTransferService } from "src/app/services/doctor-transfer.service";

@Component({
  selector: "app-doctor-details",
  templateUrl: "./doctor-details.component.html",
  styleUrls: ["./doctor-details.component.scss"],
})
export class DoctorDetailsComponent implements OnInit {
  @Input()
  doctor: Doctor;

  base64Data: any;
  retrievedImage: any;

  constructor(private doctorTransferService: DoctorTransferService) {}

  ngOnInit() {
    this.base64Data = this.doctor.photo;
    this.retrievedImage = "data:image/jpeg;base64," + this.base64Data;
  }

  onClick() {
    this.doctorTransferService.sendDoctor(this.doctor);
  }
}
