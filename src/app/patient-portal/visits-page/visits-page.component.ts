import { Component, OnInit } from "@angular/core";
import { User } from "src/app/authentication/model/user";
import { AuthenticationService } from "src/app/authentication/authentication.service";

@Component({
  selector: "app-visits-page",
  templateUrl: "./visits-page.component.html",
  styleUrls: ["./visits-page.component.scss"],
})
export class VisitsPageComponent implements OnInit {
  user: User;

  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
  }
}
