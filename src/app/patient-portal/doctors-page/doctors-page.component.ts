import { Component, OnInit } from "@angular/core";
import { DoctorService } from "src/app/services/doctor.service";
import { Doctor } from "src/app/model/doctor";

@Component({
  selector: "app-doctors-page",
  templateUrl: "./doctors-page.component.html",
  styleUrls: ["./doctors-page.component.scss"],
})
export class DoctorsPageComponent implements OnInit {
  specializations = [];
  doctors: Doctor[] = [];

  constructor(private doctorService: DoctorService) {}

  ngOnInit() {
    this.getAll();
    this.specializations.push({
      value: "ALL",
      viewValue: "All",
    });
    this.doctorService.getSpecializations().subscribe((specData) => {
      specData.forEach((specialization) =>
        this.specializations.push({
          value: specialization.toUpperCase().replace(" ", "_"),
          viewValue: specialization,
        })
      );
    });
  }

  getAll() {
    this.doctorService.getAll().subscribe((docData) => {
      this.doctors = docData;
    });
  }

  getAllBySpecialization(specialization: string) {
    this.doctorService
      .getAllBySpecialization(specialization)
      .subscribe((docData) => {
        this.doctors = docData;
      });
  }

  onSelect(value: any) {
    if (value === "ALL") {
      this.getAll();
    } else {
      this.getAllBySpecialization(value);
    }
  }
}
