import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../../authentication/authentication.service";
import { User } from "../../authentication/model/user";
import { Doctor } from "../../model/doctor";
import { DoctorService } from "../../services/doctor.service";

@Component({
  selector: "app-patient-main-page",
  templateUrl: "./patient-main-page.component.html",
  styleUrls: ["./patient-main-page.component.scss"],
})
export class PatientMainPageComponent implements OnInit {
  user: User;
  leadingDoctor: Doctor;

  constructor(
    private authenticationService: AuthenticationService,
    private doctorService: DoctorService
  ) {}

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
    this.doctorService.getLeadingDoctor(this.user.uuid).subscribe((doctor) => {
      this.leadingDoctor = doctor;
    });
  }
}
