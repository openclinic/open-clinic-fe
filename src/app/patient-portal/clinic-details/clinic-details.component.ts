import { Component, OnInit, Input } from "@angular/core";
import { Clinic } from "../../model/clinic";

@Component({
  selector: "app-clinic-details",
  templateUrl: "./clinic-details.component.html",
  styleUrls: ["./clinic-details.component.scss"],
})
export class ClinicDetailsComponent implements OnInit {
  @Input()
  clinic: Clinic;

  base64Data: any;
  retrievedImage: any;

  ngOnInit() {
    this.base64Data = this.clinic.photo;
    this.retrievedImage = "data:image/jpeg;base64," + this.base64Data;
  }
}
