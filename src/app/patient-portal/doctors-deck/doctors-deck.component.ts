import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-doctors-deck",
  templateUrl: "./doctors-deck.component.html",
  styleUrls: ["./doctors-deck.component.scss"],
})
export class DoctorsDeckComponent implements OnInit {
  @Input()
  doctors = [];

  constructor() {}

  ngOnInit() {}
}
