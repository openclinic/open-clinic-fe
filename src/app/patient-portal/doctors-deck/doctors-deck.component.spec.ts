import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorsDeckComponent } from './doctors-deck.component';

describe('DoctorsDeckComponent', () => {
  let component: DoctorsDeckComponent;
  let fixture: ComponentFixture<DoctorsDeckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorsDeckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorsDeckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
