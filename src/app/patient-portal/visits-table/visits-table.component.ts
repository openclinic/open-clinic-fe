import { Component, OnInit, Input } from "@angular/core";
import { User } from "../../authentication/model/user";
import { VisitService } from "../../services/visit.service";
import { Visit } from "../../model/visit";
import { VisitStatus } from "../../model/visitStatus";
import { DoctorService } from "../../services/doctor.service";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { MatDialog } from "@angular/material/dialog";
import { DoctorDetailsDialogComponent } from "../../dialogs/doctor-details-dialog/doctor-details-dialog.component";
import { ClinicService } from "../../services/clinic.service";
import { ClinicDetailsDialogComponent } from "../../dialogs/clinic-details-dialog/clinic-details-dialog.component";
import { PaginatedDataSource } from "../../data-source/pagination-data-source";

@Component({
  selector: "app-visits-table",
  templateUrl: "./visits-table.component.html",
  styleUrls: ["./visits-table.component.scss"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("expanded", style({ height: "*", visibility: "visible" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class VisitsTableComponent implements OnInit {
  @Input()
  user: User;

  @Input()
  fetchAll: boolean;

  expandedVisit: any;

  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty("detailRow");

  displayedColumns: string[] = [
    "Doctor",
    "Clinic",
    "Start Date",
    "End Date",
    "Description",
    "Price/USD",
    "Status",
  ];
  dataSource: any;
  endpoint: any;

  constructor(
    private visitService: VisitService,
    private doctorService: DoctorService,
    private clinicService: ClinicService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    if (this.fetchAll) {
      this.endpoint = (request) =>
        this.visitService.getAllForPatient(request, this.user.uuid);
    } else {
      this.endpoint = (request) =>
        this.visitService.getUpcoming(
          request,
          this.user.uuid,
          VisitStatus.Booked
        );
    }
    this.dataSource = new PaginatedDataSource<Visit>(this.endpoint, {
      property: "startDate",
      order: "desc",
    });
  }

  cancelVisit(visit: Visit): void {
    this.visitService
      .updateStatus(visit.uuid, VisitStatus.Canceled)
      .subscribe(() => {
        this.dataSource.fetch(this.dataSource.currentPageNumber);
      });
  }

  openDoctorDialog(visit: Visit): void {
    this.doctorService.getByUUID(visit.doctor.uuid).subscribe((doctor) => {
      this.dialog.open(DoctorDetailsDialogComponent, {
        width: "500px",
        height: "550px",
        data: doctor,
      });
    });
  }

  openClinicDialog(visit: Visit): void {
    this.clinicService.getByUUID(visit.clinic.uuid).subscribe((clinic) => {
      this.dialog.open(ClinicDetailsDialogComponent, {
        width: "500px",
        height: "500px",
        data: clinic,
      });
    });
  }

  onSort(event: any) {
    this.dataSource.sortBy({
      property: event.active,
      order: event.direction,
    });
  }
}
